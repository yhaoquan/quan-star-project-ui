
/**
 * 设置浏览器头部标题
 */
export const setTitle = function (title) {
    title = title ? `${title}—` + process.env.VUE_APP_TITLE : process.env.VUE_APP_TITLE;
    window.document.title = title;
};

/**
 * 设置html根元素的font size，用于px转rem
 */
export function setFontSize() {
    document.addEventListener('DOMContentLoaded', () => {
        // html 根元素
        const html = document.querySelector('html')
        // 获取屏幕宽度（viewport）
        const htmlWidth = window.innerWidth

        let fontSize = window.innerWidth / 10
        fontSize = fontSize > 50 ? 50 : fontSize

        // 设置html根元素的font size
        html.style.fontSize = fontSize + 'px'
    })
}

/**
 * 弹出下载框
 * @param {*} saveName 
 * @param {*} url 
 */
export function download(saveName, url) {
    if(typeof url == 'object' && url instanceof Blob) {
        url = URL.createObjectURL(url); // 创建blob地址
    }
    var aLink = document.createElement('a');
    aLink.href = url;
    aLink.target = '_blank'
    aLink.download = saveName || ''; // HTML5新增的属性，指定保存文件名，可以不要后缀，注意，file:///模式下不会生效
    var event;
    if(window.MouseEvent) {
        event = new MouseEvent('click');
    } else {
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    }
    aLink.dispatchEvent(event);
}

/**
 * 获取一级域名
 * @returns {string}
 */
export function getDomain() {
    return document.domain.split('.').slice(-2).join('.')
}

/**
 * 获取url参数
 * @param {*} url 
 * @param {*} name 
 */
export const GetQueryString = (url, name) => {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = url.substr(url.indexOf('?') + 1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}
/**
 * 获取url参数，返回JSON
 * @param {*} url 
 */
export const getParams = url => {
    const keyValueArr = url.split('?')[1].split('&')
    let paramObj = {}
    keyValueArr.forEach(item => {
        const keyValue = item.split('=')
        paramObj[keyValue[0]] = keyValue[1]
    })
    return paramObj
}

/**
 * 将对象转换URL参数字符串
 * @param {*} paramsObject 
 */
export const getParamsStr = paramsObject => {
    let params = ''
    Object.keys(paramsObject).forEach(key => {
        console.log(key, paramsObject[key]);
        params += key + '=' + paramsObject[key] + '&'
    })
    if(params.length > 0) {
        return params.substring(0, query.length-1)
    } else {
        return null
    }
}

/**
 * 获取滚动条距顶部距离
 */
export function getScrollTop() {
    return (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
}

// scrollTop animation
export const scrollTop = (el, from = 0, to, duration = 500, endCallback) => {
    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = (
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                return window.setTimeout(callback, 1000 / 60)
            }
        )
    }
    const difference = Math.abs(from - to)
    const step = Math.ceil(difference / duration * 50)

    const scroll = (start, end, step) => {
        if (start === end) {
            endCallback && endCallback()
            return
        }

        let d = (start + step > end) ? end : start + step
        if (start > end) {
            d = (start - step < end) ? end : start - step
        }

        if (el === window) {
            window.scrollTo(d, d)
        } else {
            el.scrollTop = d
        }
        window.requestAnimationFrame(() => scroll(d, end, step))
    }
    scroll(from, to, step)
}

/**
 * 数字 格式化
 * @param num
 * @param digits
 * @returns {string}
 */
export function numFormatter(num, digits) {
    const si = [
        { value: 1E18, symbol: 'E' },
        { value: 1E15, symbol: 'P' },
        { value: 1E12, symbol: 'T' },
        { value: 1E9, symbol: 'G' },
        { value: 1E6, symbol: 'M' },
        { value: 1E3, symbol: 'k' }
    ]
    for (let i = 0; i < si.length; i++) {
        if (num >= si[i].value) {
            return (num / si[i].value + 0.1).toFixed(digits).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, '$1') + si[i].symbol
        }
    }
    return num.toString()
}

/**
 * 格式化文件大小
 * @returns {string}
 */
export function formatSize(size) {
    if (size > 1024 * 1024 * 1024 * 1024) {
        return (size / 1024 / 1024 / 1024 / 1024).toFixed(2) + ' TB'
    } else if (size > 1024 * 1024 * 1024) {
        return (size / 1024 / 1024 / 1024).toFixed(2) + ' GB'
    } else if (size > 1024 * 1024) {
        return (size / 1024 / 1024).toFixed(2) + ' MB'
    } else if (size > 1024) {
        return (size / 1024).toFixed(2) + ' KB'
    }
    return size.toString() + ' B'
}

/**
 * 随机数
 * @param randomFlag 随机产生
 * @param numFlag 纯数字组合
 * @param min
 * @param max
 * @returns {string}
 */
function randomWord(randomFlag, numFlag, min, max) {
    var str = "",
        range = min,
        arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    // 随机产生
    if (randomFlag) {
        range = Math.round(Math.random() * (max - min)) + min;
    }
    //纯数字组合
    if (numFlag) {
        arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    }
    for (var i = 0; i < range; i++) {
        var pos = Math.round(Math.random() * (arr.length - 1));
        str += arr[pos];
    }
    return str;
}

/**
 * 生成6位随机数
 * @returns {string}
 */
export function getRand() {
    var timestamp = new Date().getTime();
    var nonce_str = randomWord(false, false, 6);
    return nonce_str;
}

/**
 * 生成随机len位数字
 */
export const randomLenNum = (len, date) => {
    let random = '';
    random = Math.ceil(Math.random() * 100000000000000).toString().substr(0, len ? len : 4);
    if (date) random = random + Date.now();
    return random;
};

/**
 * 获取uuid
 */
export const getUUID = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
    })
};

/**
 * 加密处理
 */
export const encryption = (params) => {
    let { data, type, param, key } = params;
    let result = JSON.parse(JSON.stringify(data));
    if (type == 'Base64') {
        param.forEach(ele => {
            result[ele] = btoa(result[ele]);
        })
    } else if (type == 'Aes') {
        param.forEach(ele => {
            result[ele] = CryptoJS.AES.encrypt(result[ele], key).toString();
        })

    }
    return result;
};





//根据个数返回相应的数组
export const getArraynum = function (len) {
    let list = [];
    for (let i = 1; i <= len; i++) {
        list.push(i);
    }
    return list;
}
/**
 * 根据身份证计算年龄，性别
 */
export const IdCard = function (UUserCard, num) {
    if (UUserCard.length == 18) {
        if (num == 1) {
            //获取出生日期
            let birth = ''
            birth = UUserCard.substring(6, 10) + "-" + UUserCard.substring(10, 12) + "-" + UUserCard.substring(12, 14);
            return birth;
        }
        if (num == 2) {
            //获取性别
            if (parseInt(UUserCard.substr(16, 1)) % 2 == 1) {
                //男
                return "1";
            } else {
                //女
                return "2";
            }
        }
        if (num == 3) {
            //获取年龄
            var myDate = new Date();
            var month = myDate.getMonth() + 1;
            var day = myDate.getDate();
            var age = myDate.getFullYear() - UUserCard.substring(6, 10) - 1;
            if (UUserCard.substring(10, 12) < month || UUserCard.substring(10, 12) == month && UUserCard.substring(12, 14) <= day) {
                age++;
            }
            return age;
        }
    }
    return '';

}
/**
 * 根据传入的list和制定的属性求和
 */
export const calcListnum = function (list, value) {
    let num = 0;
    for (let i = 0; i < list.length; i++) {
        var o = list[i];
        num = num + Number(o[value]);
    }
    return Number(num);
}
/**
 * Object的属性致空，但是属性存在
 */
export const setObjectnull = function (obj) {
    for (var o in obj) {
        obj[o] = "";
    }
    return obj;
}
/**
 * Object的属性为null的至为空字符串
 */
export const setObjectstr = function (obj) {
    for (var o in obj) {
        if (obj[o] == null || obj[o] == 'null') {
            obj[o] = "";
        }
    }
    return obj;
}
/**
 * 字符串数组转对象数组
 */
export const strCovArray = function () {
    let list = [];
    for (let i = 0; i < arguments.length; i++) {
        const str = arguments[i];
        if (!!str) {
            list.push(str);
        }
    }
    return list;
}

/**
 * 查找字符串是否存在
 */
export const findStrArray = (dic, value) => {
    if (!vaildUtil.ifnull(dic)) {
        for (let i = 0; i < dic.length; i++) {
            if (dic[i] == value) {
                return i;
                break;
            }
        }
    }
    return -1;
}
/**
 * 查找对象数组是否存在
 */
export const findObjArray = (dic, obj, v1, v2) => {
    v1 = v1 || 'value';
    v2 = v2 || 'value';
    for (let i = 0; i < dic.length; i++) {
        let o = dic[i];
        if (o[v1] == obj[v2]) {
            return i;
            break;
        }
    }
    return -1;
}

/**
 * 验证俩个对象的属性值是否相等
 */
export const validObj = (olds, news) => {
    var flag = true;
    for (var obj in news) {
        if (news[obj] != olds[obj]) {
            flag = false;
            break;
        }
    }
    return flag;
}
/**
 * 数据转换器
 */
export const dataFormat = (data, type) => {
    data = getSessionStore(data) || getStore(data) || null;
    if (vaildUtil.ifnull(data)) return undefined;
    if (type == 1) {	//转json对象
        return JSON.parse(data);
    } else if (type == 2) {//转Boolean对象
        return eval(data);
    } else {
        return data;
    }
}

//删除数组制定元素
export const removeByValue = (arr, val) => {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == val) {
            arr.splice(i, 1);
            return arr;
            break;
        }
    }
}



/**
 * @param {Object} file 从上传组件得到的文件对象
 * @returns {Promise} resolve参数是解析后的二维数组
 * @description 从Csv文件中解析出表格，解析成二维数组
 */
export const getArrayFromFile = (file) => {
    let nameSplit = file.name.split('.')
    let format = nameSplit[nameSplit.length - 1]
    return new Promise((resolve, reject) => {
        let reader = new FileReader()
        reader.readAsText(file) // 以文本格式读取
        let arr = []
        reader.onload = function (evt) {
            let data = evt.target.result // 读到的数据
            let pasteData = data.trim()
            arr = pasteData.split((/[\n\u0085\u2028\u2029]|\r\n?/g)).map(row => {
                return row.split('\t')
            }).map(item => {
                return item[0].split(',')
            })
            if (format === 'csv') resolve(arr)
            else reject(new Error('[Format Error]:你上传的不是Csv文件'))
        }
    })
}

/**
 * @param {Array} array 表格数据二维数组
 * @returns {Object} { columns, tableData }
 * @description 从二维数组中获取表头和表格数据，将第一行作为表头，用于在iView的表格中展示数据
 */
export const getTableDataFromArray = (array) => {
    let columns = []
    let tableData = []
    if (array.length > 1) {
        let titles = array.shift()
        columns = titles.map(item => {
            return {
                title: item,
                key: item
            }
        })
        tableData = array.map(item => {
            let res = {}
            item.forEach((col, i) => {
                res[titles[i]] = col
            })
            return res
        })
    }
    return {
        columns,
        tableData
    }
}

export const findNodeUpper = (ele, tag) => {
    if (ele.parentNode) {
        if (ele.parentNode.tagName === tag.toUpperCase()) {
            return ele.parentNode
        } else {
            return findNodeUpper(ele.parentNode, tag)
        }
    }
}

export const findNodeDownward = (ele, tag) => {
    const tagName = tag.toUpperCase()
    if (ele.childNodes.length) {
        let i = -1
        let len = ele.childNodes.length
        while (++i < len) {
            let child = ele.childNodes[i]
            if (child.tagName === tagName) return child
            else return findNodeDownward(child, tag)
        }
    }
}


/**
 * 判断身份证号码
 */
export function cardid(code) {
    let list = [];
    let result = true;
    let msg = '';
    var city = {
        11: "北京",
        12: "天津",
        13: "河北",
        14: "山西",
        15: "内蒙古",
        21: "辽宁",
        22: "吉林",
        23: "黑龙江 ",
        31: "上海",
        32: "江苏",
        33: "浙江",
        34: "安徽",
        35: "福建",
        36: "江西",
        37: "山东",
        41: "河南",
        42: "湖北 ",
        43: "湖南",
        44: "广东",
        45: "广西",
        46: "海南",
        50: "重庆",
        51: "四川",
        52: "贵州",
        53: "云南",
        54: "西藏 ",
        61: "陕西",
        62: "甘肃",
        63: "青海",
        64: "宁夏",
        65: "新疆",
        71: "台湾",
        81: "香港",
        82: "澳门",
        91: "国外 "
    };
    if (!validateNull(code)) {
        if (code.length == 18) {
            if (!code || !/(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(code)) {
                msg = "证件号码格式错误";
            } else if (!city[code.substr(0, 2)]) {
                msg = "地址编码错误";
            } else {
                //18位身份证需要验证最后一位校验位
                code = code.split('');
                //∑(ai×Wi)(mod 11)
                //加权因子
                var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
                //校验位
                var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2, 'x'];
                var sum = 0;
                var ai = 0;
                var wi = 0;
                for (var i = 0; i < 17; i++) {
                    ai = code[i];
                    wi = factor[i];
                    sum += ai * wi;
                }
                var last = parity[sum % 11];
                if (parity[sum % 11] != code[17]) {
                    msg = "证件号码校验位错误";
                } else {
                    result = false;
                }

            }
        } else {
            msg = "证件号码长度不为18位";
        }

    } else {
        msg = "证件号码不能为空";
    }
    list.push(result);
    list.push(msg);
    return list;
}

/**
 * 格式化日期
 * @param time 日期时间（字符串，时间戳，日期对象）
 * @param cFormat 格式，默认（yyyy-MM-dd HH:mm:ss）
 * @returns {*}
 */
export function parseTime(time, cFormat) {
    if (time) {
        if (arguments.length === 0) {
            return null
        }

        const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'

        let date
        if (typeof time === 'object') {
            date = time
        } else if (typeof time === 'string') {
            date = new Date(new Date(time).getTime())
        } else if (typeof time === 'number') {
            date = new Date(time)
        } else {
            date = new Date(parseInt(time))
        }

        const formatObj = {
            y: date.getFullYear(),
            m: date.getMonth() + 1,
            d: date.getDate(),
            h: date.getHours(),
            i: date.getMinutes(),
            s: date.getSeconds(),
            a: date.getDay()
        }
        const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
            let value = formatObj[key]
            if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][(value - 1 === -1 ? 6 : value - 1)]
            if (result.length > 0 && value < 10) {
                value = '0' + value
            }
            return value || 0
        })
        return time_str
    } else {
        return '-'
    }

}

/**
 * 将日期格式化成指定格式的字符串
 * @param date 要格式化的日期，不传时默认当前时间，也可以是一个时间戳
 * @param fmt 目标字符串格式，支持的字符有：y,M,d,q,w,H,h,m,S，默认：yyyy-MM-dd HH:mm:ss
 * @returns 返回格式化后的日期字符串
 */
export function formatDate(date, fmt) {
    date = date == undefined ? new Date() : date;
    date = typeof date == 'number' ? new Date(date) : date;
    fmt = fmt || 'yyyy-MM-dd HH:mm:ss';
    var obj =
    {
        'y': date.getFullYear(), // 年份，注意必须用getFullYear
        'M': date.getMonth() + 1, // 月份，注意是从0-11
        'd': date.getDate(), // 日期
        'q': Math.floor((date.getMonth() + 3) / 3), // 季度
        'w': date.getDay(), // 星期，注意是0-6
        'H': date.getHours(), // 24小时制
        'h': date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, // 12小时制
        'm': date.getMinutes(), // 分钟
        's': date.getSeconds(), // 秒
        'S': date.getMilliseconds() // 毫秒
    };
    var week = ['天', '一', '二', '三', '四', '五', '六'];
    for (var i in obj) {
        fmt = fmt.replace(new RegExp(i + '+', 'g'), function (m) {
            var val = obj[i] + '';
            if (i == 'w') return (m.length > 2 ? '星期' : '周') + week[val];
            for (var j = 0, len = val.length; j < m.length - len; j++) val = '0' + val;
            return m.length == 1 ? val : val.substring(val.length - m.length);
        });
    }
    return fmt;
}

/**
 * 计算两个日期之间的工作日天数
 * @param {Date} startDate 
 * @param {Date} endDate 
 */
export function getWorkDays(startDate, endDate) {
    var startDate = new Date(startDate.replace(/-/g, "/"));
    var endDate = new Date(endDate.replace(/-/g, "/"));

    var diffDays = (endDate - startDate) / (1000 * 60 * 60 * 24) + 1;//获取日期之间相差的天数
    var remainDay = diffDays % 7;//得到日期之间的余数（0-6之间）
    var weeks = Math.floor(diffDays / 7);//获取日期之间有多少周
    var weekends = 2 * weeks;//计算每周*2 得到取整的的周六日天数
    var weekDay = startDate.getDay();//获取开始日期为星期几（0，1，2，3，4，5，6）0对应星期日
    for (var i = 0; i < remainDay; i++) {//循环处理余下的天数有多少个周六或者周日（最多出现一个周六或者一个周日）
        if (((weekDay + i) == 6) || ((weekDay + i) == 0) || ((weekDay + i) == 7)) {
            weekends = weekends + 1;
        }
    }
    return (diffDays - weekends);//工作日=相差天数减去周六日天数
}


/**
 * 计算年的时间差
 */
export const nowCovyearOld = function (date) {
    return dateFormat(new Date(), 'yyyy') - date;
}
/**
 * 计算到当年年份的时间差
 */
export const nowCri = function (date) {
    if (new Date().getTime() - new Date(date).getTime() < 0) return true;
    return false;
}
/**
 * 计算年俩个年份的时间差
 */
export const dateCri = function (date1, date2) {
    if (new Date(date2).getTime() - new Date(date1).getTime() < 0) return true;
    return false;
}

/**
 * 计算年龄
 * @param {*} date 
 */
export const getAge = (date) => {
    var birthdays = new Date(date.replace(/-/g, "/"));
    var d = new Date();
    var age = 
        d.getFullYear() 
        - birthdays.getFullYear() 
        - (d.getMonth() < birthdays.getMonth() 
        || (d.getMonth() == birthdays.getMonth() 
        && d.getDate() < birthdays.getDate()) ? 1 : 0);
    return age
}

/**
 * 计算剩余时间
 * 返回{天,时,分,秒}
 * @param {*} date1 
 * @param {*} date2 
 */
export const calcDate = (date1, date2) => {
    var date3 = date2 - date1;

    var days = Math.floor(date3 / (24 * 3600 * 1000))

    var leave1 = date3 % (24 * 3600 * 1000) //计算天数后剩余的毫秒数  
    var hours = Math.floor(leave1 / (3600 * 1000))

    var leave2 = leave1 % (3600 * 1000) //计算小时数后剩余的毫秒数  
    var minutes = Math.floor(leave2 / (60 * 1000))

    var leave3 = leave2 % (60 * 1000) //计算分钟数后剩余的毫秒数  
    var seconds = Math.round(date3 / 1000)
    return {
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds,
    }
}


/**
 * JSON对象转成QueryString
 * @param {*} json 
 */
export const jsonToQueryString = (json) => {
    if (!json) return ''
    return cleanArray(Object.keys(json).map(key => {
        if (json[key] === undefined || json[key] === '' || json[key] === null) return ''
        return encodeURIComponent(key) + '=' + encodeURIComponent(json[key])
    })).join('&')
}
const cleanArray = (actual) => {
    const newArray = []
    for (let i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i])
        }
    }
    return newArray
}
