import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/product/pmsattrattrgrouprelation/save',
        method: 'post',
        data
    })
}

/**
 * 删除或批量删除
 * @param {*} data
 */
export const deleted = (data) => {
    return http({
        url: '/product/pmsattrattrgrouprelation/delete',
        method: 'post',
        data
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/product/pmsattrattrgrouprelation/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/product/pmsattrattrgrouprelation/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/product/pmsattrattrgrouprelation/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/product/pmsattrattrgrouprelation/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/product/pmsattrattrgrouprelation/selectMyPage',
        method: 'get',
        params: query
    })
}

