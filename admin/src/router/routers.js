import Main from '@/views/main/main.vue'
import ParentView from '@/components/parent-view/parent-view.vue'
import IFrame from '@/components/iframe/iframe.vue'
import HotelRouter from '@/router/hotel'
import SysRouter from '@/router/sys'
import DemoRouter from '@/router/demo'

/**
 * meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 *  href: 'https://xxx' (default: null) 用于跳转到外部连接
 *  iframe: false  用于跳转到外部连接，打开新的页面
 *  iframe: true  用于跳转到外部连接，在tab中打开外部连接，需设置component: IFrame
 * }
 */



const Login = {
    path: '/login',
    name: 'login',
    meta: {
        title: '登录',
        hideInMenu: true
    },
    component: () => import('@/views/login/login.vue')
}

const Forget = {
    path: '/forget',
    name: 'forget',
    meta: {
        title: '密码找回',
        hideInMenu: true
    },
    component: () => import('@/views/login/forget.vue')
}

const Profile = {
    path: '/profile',
    name: '_profile',
    meta: {
        title: '个人信息',
        hideInMenu: true
    },
    component: Main,
    children: [
        {
            path: '/profile',
            name: 'profile',
            meta: {
                title: '我的信息',
                hideInMenu: false
            },
            component: () => import('@/views/profile/index.vue')
        }
    ]
}
const Home = {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
        hideInMenu: true
    },
    children: [
        {
            path: '/home',
            name: 'home',
            meta: {
                hideInMenu: true,
                title: '首页',
                icon: 'md-home'
            },
            component: () => import('@/views/dashboard/dashboard.vue')
        }
    ]
}

const Unauthorized = {
    path: '/401',
    name: '401',
    meta: {
        hideInMenu: true
    },
    component: () => import('@/views/errors/error-401.vue')
}
const ServerError = {
    path: '/500',
    name: '500',
    meta: {
        hideInMenu: true
    },
    component: () => import('@/views/errors/error-500.vue')
}

//最后添加到路由中
const NotFoundPage = {
    path: '*',
    name: '404',
    meta: {
        hideInMenu: true
    },
    component: () => import('@/views/errors/error-404.vue')
}

export const BaseRouters = [
    Login,
    Forget,
    Profile,
    Home
]

export default [...BaseRouters, ...HotelRouter, ...SysRouter, ...DemoRouter, NotFoundPage]

