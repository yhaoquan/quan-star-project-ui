import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/syssettings/save',
        method: 'post',
        data
    })
}

/**
 * 删除
 * @param {*} idList idList: [1,2,3]
 */
export const deleted = (idList) => {
    return http({
         url: '/syssettings/delete',
        method: 'post',
        data: idList
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/syssettings/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/syssettings/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/syssettings/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/syssettings/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/syssettings/selectMyPage',
        method: 'get',
        params: query
    })
}

