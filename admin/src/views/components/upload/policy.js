import http from '@/libs/http'

export function policy() {
   return  new Promise((resolve,reject)=>{
        http({
            url: '/thirdparty/oss/policy',
            method: "get"
        }).then(data => {
            resolve(data);
        })
    });
}