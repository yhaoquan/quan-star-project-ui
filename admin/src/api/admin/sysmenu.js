import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/sysmenu/save',
        method: 'post',
        data
    })
}

/**
 * 删除
 * @param {*} idList idList: [1,2,3]
 */
export const deleted = (idList) => {
    return http({
         url: '/sysmenu/delete',
        method: 'post',
        data: idList
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/sysmenu/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/sysmenu/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/sysmenu/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/sysmenu/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/sysmenu/selectMyPage',
        method: 'get',
        params: query
    })
}

/**
 * 查询所有并组装为树状结构
 */
export function listWithTree() {
    return http({
        url: '/sysmenu/listWithTree',
        method: 'get'
    })
}
/**
 * 查询所有，组装为树状结构，并加载对应的权限
 */
export function listWithTreeOnPermission() {
    return http({
        url: '/sysmenu/listWithTreeOnPermission',
        method: 'get'
    })
}