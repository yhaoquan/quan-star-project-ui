import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { getToken } from "@/libs/auth";
import { Notification, MessageBox, Message, Loading } from 'element-ui'

// 创建Axios实例
const axiosInstance = axios.create({
    baseURL: process.env.VUE_APP_BASE_SERVICE_URL, // API的base_url+加前缀
    timeout: 80000, // 请求超时时间
    withCredentials: true // 跨域请求，允许保存cookie
})

// request拦截器
axiosInstance.interceptors.request.use(config => {
    // 发送请求前
    if(getToken()) {
        config.headers['token'] = getToken() // 设置默认携带的头信息(Token)
    }
    return config
}, error => {
    // 请求错误
    return Promise.reject(error);
})

//防止重复弹出“重新登陆”对话框
let flag = false

// response拦截器
axiosInstance.interceptors.response.use(
    response => {
        // 响应结果返回到页面前可以对数据进行处理，自定义code来标示请求状态，当code返回如下情况为权限有问题，登出并返回到登录页
        const data = response.data
        if (data.code === 401 && !flag) { //Token过期，需重新登录
            flag = true

            // 登录后返回原来的页面的URL
            let url = router.history.current.path
            // 登录后返回原来的页面的URL参数
            let query = router.history.current.query

            store.dispatch('FedLogOut', {url, query}).then(() => {
                MessageBox.alert(data.msg, '警告', {
                    confirmButtonText: '重新登陆',
                    callback: () => {
                        Loading.service({
                            lock: true,
                            text: '正在退出，请稍等...',
                            background: 'rgba(0, 0, 0, 0.7)'
                        });
                        setTimeout(() => {
                            window.location.reload()
                        }, 500)

                    }
                })
            })
            return response
        }
        return response.data
    },
    error => {
        // 请求已发出，但服务器响应的状态码不在 2xx 范围内
        if (error.response) {
            switch (error.response.status) {
                case 400:
                    Msg('error', error.response.status + ' 错误的请求')
                    break
                case 404:
                    Msg('error', error.response.status + ' 路径错误')
                    break
                case 500:
                    ExceptionMsg('', '服务器异常-' + error.response.status, error.response.data.message)
                    break
                default:
                    Msg('error', error.response.status + '服务器被吃了⊙﹏⊙∥')
                    break
            }
        } else {
            console.log('请求发生错误==》', error)
            Msg('error', '未知错误!')
        }

        return Promise.reject(error)
    }
)

function Msg(type, message) {
    Message({
        type: type,
        duration: 2000,
        message: message
    })
}

function NotificationMsg(type, title, message) {
    Notification({
        type: type,
        duration: 2000,
        title: title,
        message: message
    })
}

function ExceptionMsg(type, title, message) {
    MessageBox({
        type: type,
        title: title,
        message: message,
        customClass: 'errorMessageBox'
    })
}

export default axiosInstance;