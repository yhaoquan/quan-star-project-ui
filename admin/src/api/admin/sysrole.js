import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/sysrole/save',
        method: 'post',
        data
    })
}

/**
 * 删除
 * @param {*} idList idList: [1,2,3]
 */
export const deleted = (idList) => {
    return http({
         url: '/sysrole/delete',
        method: 'post',
        data: idList
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/sysrole/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/sysrole/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/sysrole/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/sysrole/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/sysrole/selectMyPage',
        method: 'get',
        params: query
    })
}

/**
 * 根据角色ID获取对应的权限
 * @param {*} roleId 
 */
export const getPermissionByRoleId = (roleId) => {
    return http({
        url: '/sysrole/getPermissionByRoleId/'+roleId,
        method: 'get'
    })
}

/**
 * 保存角色对应的菜单权限
 * @param {*} data 
 */
export const saveRolePermission = (data) => {
    return http({
        url: '/sysrole/saveRolePermission',
        method: 'post',
        data
    })
}

