import Main from '@/views/main/main.vue';
import ParentView from '@/components/parent-view/parent-view.vue';
import IFrame from '@/components/iframe/iframe.vue';

/**
 * meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 *  href: 'https://xxx' (default: null) 用于跳转到外部连接
 *  iframe: false  用于跳转到外部连接，打开新的页面
 *  iframe: true  用于跳转到外部连接，在tab中打开外部连接，需设置component: IFrame
 * }
 */

export default [
    {
        path: '/admin',
        name: 'admin',
        meta: {
            icon: 'logo-buffer',
            title: '系统管理',
            access: ['admin:page']
        },
        component: Main,
        children: [
            {
                path: 'user',
                name: 'user',
                meta: {
                    icon: 'logo-buffer',
                    title: '用户管理',
                    access: ['admin:systemuser:page']
                },
                component: () => import('@/views/admin/sysuser/index.vue')
            },
            {
                path: 'role',
                name: 'role',
                meta: {
                    icon: 'logo-buffer',
                    title: '角色管理',
                    access: ['admin:systemrole:page']
                },
                component: () => import('@/views/admin/sysrole/index.vue')
            },
            {
                //子页面，不在菜单和面包屑中显示
                path: 'permission',
                name: 'permission',
                meta: {
                    icon: 'logo-buffer',
                    title: '权限',
                    hideInBread: true,
                    hideInMenu: true
                },
                component: () =>
                    import('@/views/admin/sysrole/permission/index.vue')
            },
            {
                path: 'menu',
                name: 'menu',
                meta: {
                    icon: 'logo-buffer',
                    title: '菜单管理',
                    access: ['admin:systemmenu:page']
                },
                component: () => import('@/views/admin/sysmenu/index.vue')
            },
            {
                //子页面，不在菜单和面包屑中显示
                path: 'initPermission',
                name: 'initPermission',
                meta: {
                    icon: 'logo-buffer',
                    title: '权限标识',
                    hideInBread: true,
                    hideInMenu: true
                },
                component: () =>
                    import(
                        '@/views/admin/sysmenu/permission/initPermission.vue'
                    )
            },
            {
                path: 'message',
                name: 'message',
                meta: {
                    icon: 'logo-buffer',
                    title: '消息管理',
                    access: ['admin:systemmessage:page']
                },
                component: () => import('@/views/admin/sysmessage/index.vue')
            },
            {
                path: 'dict',
                name: 'dict',
                meta: {
                    icon: 'logo-buffer',
                    title: '字典管理',
                    access: ['admin:systemdict:page']
                },
                component: () => import('@/views/admin/sysdict/index.vue')
            },
            {
                path: 'file',
                name: 'file',
                meta: {
                    icon: 'logo-buffer',
                    title: '文件管理',
                    access: ['admin:systemfile:page']
                },
                component: () => import('@/views/admin/sysfile/index.vue')
            },
            {
                path: 'scheduler',
                name: 'scheduler',
                meta: {
                    icon: 'logo-buffer',
                    title: '定时任务',
                    access: ['admin:systemfile:page']
                },
                component: () =>
                    import('@/views/admin/sysschedulerjob/index.vue')
            },
            {
                path: 'log',
                name: 'log',
                meta: {
                    icon: 'logo-buffer',
                    title: '日志管理',
                    access: ['admin:systemlog:page']
                },
                component: () => import('@/views/admin/syslog/index.vue')
            },
            {
                path: 'settings',
                name: 'settings',
                meta: {
                    icon: 'logo-buffer',
                    title: '参数设置',
                    access: ['admin:systemsettings:page']
                },
                component: () => import('@/views/admin/syssettings/index.vue')
            },
            {
                path: 'generator',
                name: 'generator',
                meta: {
                    icon: 'logo-buffer',
                    title: '代码生成',
                    access: ['admin:systemgenerator:page']
                },
                component: () => import('@/views/admin/generator/index.vue')
            },
            {
                path: 'monitor',
                name: 'monitor',
                meta: {
                    icon: 'md-funnel',
                    title: '系统监控',
                    access: ['admin:systemmonitor:page']
                },
                component: ParentView,
                children: [
                    {
                        name: 'rabbitmq',
                        path: 'rabbitmq',
                        meta: {
                            icon: 'logo-buffer',
                            title: '消息队列',
                            href: 'http://127.0.0.1:15672',
                            iframe: false
                        },
                        component: IFrame
                    }
                ]
            }
        ]
    }
];
