import { setStore, getStore, removeStore } from "@/libs/cache";
import store from '@/store'
import { hasOneOf } from '@/libs/tools'


const TokenKey = 'token'

export function setToken(token) {
    setStore({ name: TokenKey, content: token, type: 'session' })
}

export function getToken() {
    return getStore({ name: TokenKey })
}

export function removeToken() {
    removeStore({ name: TokenKey })
}


/**
 * 操作按钮权限
 * @param {*} access 用户权限数组，如 ['system:user:add', 'system:user:deleted']
 */
export const isAuth = (access) => {
    if(store.getters.isAdmin) {
        return true
    } else {
        if (store.getters.access) return hasOneOf(store.getters.access, access)
        else return true
    }
}

