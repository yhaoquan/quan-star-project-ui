import Vue from 'vue'
import Vuex from 'vuex'
import app from '@/store/module/app'
import user from '@/store/module/user'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {

    },
    mutations: {

    },
    actions: {

    },
    modules: {
        app,
        user
    }
})