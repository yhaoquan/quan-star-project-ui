import { 
    getBreadCrumbList, 
    getHomeRoute, 
    routeHasExist, 
    setTagNavListInLocalstorage, 
    getTagNavListFromLocalstorage,
    getMenuByRouter
 } from '@/libs/router_utils.js'
import routers from '@/router/routers'

export default {
    state: {
        sidebar: false,
        tagNavList: [],
        breadCrumbList: [],
        homeRoute: getHomeRoute(routers),
    },

    getters: {
        sidebar: state => state.sidebar,
        menus: (state, getters, rootState) => getMenuByRouter(routers, rootState.user.access, rootState.user.isAdmin)
    },

    mutations: {
        setToggleSidebar: (state) => {
            state.sidebar = !state.sidebar
        },
        setBreadCrumb(state, routeMetched) {
            state.breadCrumbList = getBreadCrumbList(routeMetched, state.homeRoute)
        },
        setTagNavList(state, list) {
            if (list) {
                let isHideInBread = list[list.length-1].meta.hideInBread
                if(!isHideInBread) {
                    state.tagNavList = [...list]
                    setTagNavListInLocalstorage([...list])
                }
            } else state.tagNavList = getTagNavListFromLocalstorage()
        },
        addTag(state, { route, type = 'unshift' }) {
            if (!routeHasExist(state.tagNavList, route)) {
                if (type === 'push') state.tagNavList.push(route)
                else {
                    if (route.name === 'dashboard') state.tagNavList.unshift(route)
                    else state.tagNavList.splice(1, 0, route)
                }
                setTagNavListInLocalstorage([...state.tagNavList])
            }
        }
    },

    actions: {
        handlerToggleSideBar({ commit, state }) {
            commit('setToggleSidebar')
        }
    }
}