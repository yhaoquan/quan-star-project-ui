module.exports = {
    lintOnSave: false,
    // 配置二级域名，http://quancloud.com/admin，对应nginx /admin
    // publicPath:"/admin",
    devServer: {
        port: 6699,
        disableHostCheck: true
    }

}