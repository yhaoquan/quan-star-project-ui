import Cookies from 'js-cookie'
import { Base64 } from 'js-base64'
import { getDomain } from "@/libs/utils.js";

const TOKEN = 'token'
const REMEMBER = 'remember'

/**
 * 获取用户登录令牌
 * @returns {*}
 */
export function getToken() {
    return Cookies.get(TOKEN)
}

/**
 * 设置用户登录令牌
 * @param token
 * @returns {*}
 */
export function setToken(token) {
    return Cookies.set(TOKEN, token, {domain: '.' + getDomain()})
}

/**
 * 删除用户登录令牌
 * @returns {*}
 */
export function removeToken() {
    Cookies.remove(TOKEN, {domain: '.' + getDomain()})
}

/**
 * 记住用户账号和密码
 * @param account 账号
 * @param password 密码
 */
export function setRememberAccountInfo(account, password) {
    const rememberAccount = Base64.encode(account + '##' + password)
    Cookies.set(REMEMBER, rememberAccount, {expires: 7})
}

/**
 * 获取用户账号密码
 * @returns 数组（0：账号，1：密码）
 */
export function getRememberAccountInfo() {
    const remember = Cookies.get(REMEMBER)
    if(remember) {
        return Base64.decode(remember).split('##')
    } else {
        return null
    }

}

/**
 * 删除用户账号信息
 */
export function removeRememberAccountInfo() {
    Cookies.remove(REMEMBER)
}