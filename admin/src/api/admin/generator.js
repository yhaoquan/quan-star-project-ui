import http from '@/libs/http'

/**
 * 获取数据库
 * @param {*} query 
 */
export const database = (query) => {
    return http({
        url: '/generator/database',
        method: 'get',
        params: query
    })
}

/**
 * 获取数据库表
 * @param {*} query 
 */
export const tables = (query) => {
    return http({
        url: '/generator/tables',
        method: 'get',
        params: query
    })
}

/**
 * 代码生成
 * @param {*} query 
 */
export const code = (query) => {
    return http({
        url: '/generator/code',
        method: 'get',
        params: query
    })
}
