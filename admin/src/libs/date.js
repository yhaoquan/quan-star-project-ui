/**
 * 格式化日期
 * @param time 日期时间（字符串，时间戳，日期对象）
 * @param cFormat 格式，默认（yyyy-MM-dd HH:mm:ss）
 * @returns {*}
 */
export function parseTime(time, cFormat) {
    if (time) {
        if (arguments.length === 0) {
            return null
        }

        const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'

        let date
        if (typeof time === 'object') {
            date = time
        } else if (typeof time === 'string') {
            date = new Date(new Date(time).getTime())
        } else if (typeof time === 'number') {
            date = new Date(time)
        } else {
            date = new Date(parseInt(time))
        }

        const formatObj = {
            y: date.getFullYear(),
            m: date.getMonth() + 1,
            d: date.getDate(),
            h: date.getHours(),
            i: date.getMinutes(),
            s: date.getSeconds(),
            a: date.getDay()
        }
        const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
            let value = formatObj[key]
            if (key === 'a') return ['一', '二', '三', '四', '五', '六', '日'][(value - 1 === -1 ? 6 : value - 1)]
            if (result.length > 0 && value < 10) {
                value = '0' + value
            }
            return value || 0
        })
        return time_str
    } else {
        return '-'
    }

}

export function formatTime(time, option) {
    time = +time * 1000
    const d = new Date(time)
    const now = Date.now()
  
    const diff = (now - d) / 1000
  
    if (diff < 30) {
      return '刚刚'
    } else if (diff < 3600) { // less 1 hour
      return Math.ceil(diff / 60) + '分钟前'
    } else if (diff < 3600 * 24) {
      return Math.ceil(diff / 3600) + '小时前'
    } else if (diff < 3600 * 24 * 2) {
      return '1天前'
    }
    if (option) {
      return parseTime(time, option)
    } else {
      return d.getMonth() + 1 + '月' + d.getDate() + '日' + d.getHours() + '时' + d.getMinutes() + '分'
    }
  }

/**
 * 将日期格式化成指定格式的字符串
 * @param date 要格式化的日期，不传时默认当前时间，也可以是一个时间戳
 * @param fmt 目标字符串格式，支持的字符有：y,M,d,q,w,H,h,m,S，默认：yyyy-MM-dd HH:mm:ss
 * @returns 返回格式化后的日期字符串
 */
export function formatDate(date, fmt) {
    date = date == undefined ? new Date() : date;
    date = typeof date == 'number' ? new Date(date) : date;
    fmt = fmt || 'yyyy-MM-dd HH:mm:ss';
    var obj =
    {
        'y': date.getFullYear(), // 年份，注意必须用getFullYear
        'M': date.getMonth() + 1, // 月份，注意是从0-11
        'd': date.getDate(), // 日期
        'q': Math.floor((date.getMonth() + 3) / 3), // 季度
        'w': date.getDay(), // 星期，注意是0-6
        'H': date.getHours(), // 24小时制
        'h': date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, // 12小时制
        'm': date.getMinutes(), // 分钟
        's': date.getSeconds(), // 秒
        'S': date.getMilliseconds() // 毫秒
    };
    var week = ['天', '一', '二', '三', '四', '五', '六'];
    for (var i in obj) {
        fmt = fmt.replace(new RegExp(i + '+', 'g'), function (m) {
            var val = obj[i] + '';
            if (i == 'w') return (m.length > 2 ? '星期' : '周') + week[val];
            for (var j = 0, len = val.length; j < m.length - len; j++) val = '0' + val;
            return m.length == 1 ? val : val.substring(val.length - m.length);
        });
    }
    return fmt;
}

/**
 * 计算两个日期之间的工作日天数
 * @param {Date} startDate 
 * @param {Date} endDate 
 */
export function getWorkDays(startDate, endDate) {
    var startDate = new Date(startDate.replace(/-/g, "/"));
    var endDate = new Date(endDate.replace(/-/g, "/"));

    var diffDays = (endDate - startDate) / (1000 * 60 * 60 * 24) + 1;//获取日期之间相差的天数
    var remainDay = diffDays % 7;//得到日期之间的余数（0-6之间）
    var weeks = Math.floor(diffDays / 7);//获取日期之间有多少周
    var weekends = 2 * weeks;//计算每周*2 得到取整的的周六日天数
    var weekDay = startDate.getDay();//获取开始日期为星期几（0，1，2，3，4，5，6）0对应星期日
    for (var i = 0; i < remainDay; i++) {//循环处理余下的天数有多少个周六或者周日（最多出现一个周六或者一个周日）
        if (((weekDay + i) == 6) || ((weekDay + i) == 0) || ((weekDay + i) == 7)) {
            weekends = weekends + 1;
        }
    }
    return (diffDays - weekends);//工作日=相差天数减去周六日天数
}


/**
 * 计算年的时间差
 */
export const nowCovyearOld = function (date) {
	return dateFormat(new Date(), 'yyyy') - date;
}
/**
 * 计算到当年年份的时间差
 */
export const nowCri = function (date) {
	if (new Date().getTime() - new Date(date).getTime() < 0) return true;
	return false;
}
/**
 * 计算年俩个年份的时间差
 */
export const dateCri = function (date1, date2) {
	if (new Date(date2).getTime() - new Date(date1).getTime() < 0) return true;
	return false;
}

/**
 * 计算已过去时间
 * @param {*} date1 
 * @param {*} date2 
 */
export const calcDate = (date1, date2) => {
    var date3 = date2 - date1;

    var days = Math.floor(date3 / (24 * 3600 * 1000))

    var leave1 = date3 % (24 * 3600 * 1000) //计算天数后剩余的毫秒数  
    var hours = Math.floor(leave1 / (3600 * 1000))

    var leave2 = leave1 % (3600 * 1000) //计算小时数后剩余的毫秒数  
    var minutes = Math.floor(leave2 / (60 * 1000))

    var leave3 = leave2 % (60 * 1000) //计算分钟数后剩余的毫秒数  
    var seconds = Math.round(date3 / 1000)
    return {
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds,
    }
}

/**
 * 判断哪一年是否闰年
 * @param {*} year 
 */
export function isLeapYear (year) {
    return  year % 4 === 0 && year % 100 !== 0 ? 1 : (year % 400 === 0 ? 1 : 0);
}

/**
 * 某年某月月份天数
 * @param {*} year 
 * @param {*} month 
 */
export function monthDates (year, month) {
    return new Date(year,month,0).getDate();
}

/**
 * 任意一天的下一天  为节省代码，本函数不做无意义的校验，输入的天数必须小于等于当月天数，调用时自行
 * @param {*} year 
 * @param {*} month 
 * @param {*} date 
 */
export function nextDate (year, month, date) {
    return date === this.monthDates(year,month) ? [month === 12 ? year + 1 : year, month === 12 ? 1 : month + 1, 1] : [year, month, date + 1];
}

/**
 * 任意一月往前推若干个月
 * @param {*} year 
 * @param {*} month 
 * @param {*} num 
 */
export function preMonths (year, month, num) {
    return month < num ? [year - Math.floor(num / 12), month + (num % 12)]: [year,month-num];
}

/**
 * 任意一月往后推若干个月
 * @param {*} year 
 * @param {*} month 
 * @param {*} num 
 */
export function afterMonths (year, month, num) {
    return (month + num) > 12 ? ( month + num % 12 > 12 ? [year + Math.floor(num / 12) + 1, month + (num % 12) - 12] : [year + Math.floor(num / 12), month + (num % 12)] ) : [year, month + num];
}

/**
 * 若干月前
 * @param {*} num 
 */
export function someMonthsBefore (num) {
    var date = new Date(),
        year = date.getFullYear(),
        month = date.getMonth() + 1;
    return this.dateFormat(this.preMonths(year, month, num),"-");
}

/**
 * 若干月后
 * @param {*} num 
 */
export function someMonthsLater (num) {
    var date = new Date(),
        year = date.getFullYear(),
        month = date.getMonth() + 1;
    return this.dateFormat(this.afterMonths(year, month, num),"-");
}