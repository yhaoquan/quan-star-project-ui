/**
 * 复制功能
 */

import Vue from 'vue'
import Clipboard from 'clipboard'

function clipboardSuccess() {
    Vue.prototype.$message({
        message: 'Copy successfully',
        type: 'success',
        duration: 1500
    })
}

function clipboardError() {
    Vue.prototype.$message({
        message: 'Copy failed',
        type: 'error'
    })
}

export default function handleClipboard(text, event) {
    const clipboard = new Clipboard(event.target, {
        text: () => text
    })
    clipboard.on('success', () => {
        clipboardSuccess()
        clipboard.off('error')
        clipboard.off('success')
        clipboard.destroy()
    })
    clipboard.on('error', () => {
        clipboardError()
        clipboard.off('error')
        clipboard.off('success')
        clipboard.destroy()
    })
    clipboard.onClick(event)
}
// 使用方式
// <el-input placeholder="请输入内容" v-model="qrCode.activityUrl" class="input-with-select">
//     <el-button slot="append" @click="handleClipboard(qrCode.activityUrl, $event)">复制</el-button>
// </el-input>