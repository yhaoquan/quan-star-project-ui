@charset "UTF-8";
/******************************************************************
*** @mixin 指令
*******************************************************************/
@font-face {
  font-family: Ionicons;
  src: url(/assets/styles/fonts/ionicons.ttf?v=3.0.0) format("truetype"), url(/assets/styles/fonts/ionicons.woff?v=3.0.0) format("woff"), url(/assets/styles/fonts/ionicons.svg?v=3.0.0#Ionicons) format("svg");
  font-weight: 400;
  font-style: normal;
}

/******************************************************************
*** @function 函数
*******************************************************************/
/**
 px 转 rem
 1rem = fontSize px
 1px = (1 / fontSize)rem
*/
/******************************************************************
*** Variables $ 变量
*******************************************************************/
/******************************************************************
*** 内外边距
*******************************************************************/
.margin-top-8 {
  margin-top: 8px;
}

.margin-top-10 {
  margin-top: 10px;
}

.margin-top-20 {
  margin-top: 20px;
}

.margin-left-5 {
  margin-left: 5px;
}

.margin-left-10 {
  margin-left: 10px;
}

.margin-bottom-5 {
  margin-bottom: 5px;
}

.margin-bottom-10 {
  margin-bottom: 10px;
}

.margin-bottom-20 {
  margin-bottom: 20px;
}

.margin-bottom-100 {
  margin-bottom: 100px;
}

.margin-right-5 {
  margin-right: 5px;
}

.margin-right-10 {
  margin-right: 10px;
}

.padding-left-6 {
  padding-left: 6px;
}

.padding-left-8 {
  padding-left: 5px;
}

.padding-left-10 {
  padding-left: 10px;
}

.padding-left-20 {
  padding-left: 20px;
}

.height-100 {
  height: 100%;
}

.height-120px {
  height: 100px;
}

.height-200px {
  height: 200px;
}

.height-492px {
  height: 492px;
}

.height-460px {
  height: 460px;
}

.line-gray {
  height: 0;
  border-bottom: 1px solid #dcdcdc;
}

.notwrap {
  word-break: keep-all;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.padding-left-5 {
  padding-left: 10px;
}

.lh-30 {
  line-height: 30px;
}

.lh-35 {
  line-height: 35px;
}

.lh-38 {
  line-height: 37px;
}

.lh-40 {
  line-height: 40px;
}

.lh-45 {
  line-height: 45px;
}

.no-padding {
  padding: 0;
}

.no-margin {
  margin: 0;
}

body .el-table th.gutter {
  display: table-cell !important;
}

.layout-main {
  display: flex;
}

.layout-main.hidderSidebar .layout-sidebar-container {
  width: 64px;
}

.layout-main.hidderSidebar .logo_wrapper {
  width: 64px;
  transition: all 0.28s ease-out;
}

.layout-main.hidderSidebar .layout-container {
  padding-left: 64px;
}

.layout-main .logo_wrapper {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 64px;
  width: 200px;
  font-size: 22px;
  font-weight: bold;
  background: #304156;
  transition: all 0.28s ease-out;
}

.layout-main .layout-sidebar-container {
  background: #304156;
  position: fixed;
  top: 0;
  bottom: 0;
  width: 200px;
  overflow: hidden;
  color: #fff;
  overflow-y: auto;
  z-index: 1000;
  transition: all 0.28s ease-out;
  margin-top: 64px;
}

.layout-main .layout-sidebar-container::-webkit-scrollbar {
  display: none;
}

.layout-main .layout-sidebar-container .el-menu {
  width: 100% !important;
  min-height: 100%;
  border-right: none;
  box-sizing: border-box;
}

.layout-main .layout-sidebar-container .el-submenu .el-menu-item {
  min-width: 180px !important;
  background-color: #1f2d3d !important;
}

.layout-main .layout-sidebar-container .el-submenu .el-menu-item:hover {
  background-color: #001528 !important;
}

.layout-main .layout-container {
  box-sizing: border-box;
  position: absolute;
  top: 0;
  bottom: 0;
  padding-left: 200px;
  width: 100%;
  transition: all 0.28s ease-out;
}

.layout-main .layout-container .layout-header-container {
  display: flex;
  height: 64px;
  background: #fff;
}

.layout-main .layout-container .layout-header-container .header-left {
  flex: 1;
  display: flex;
  justify-content: flex-start;
  align-items: center;
}

.layout-main .layout-container .layout-header-container .header-left .sidebar-trigger {
  box-sizing: border-box;
  align-self: center;
  padding: 0 10px;
}

.layout-main .layout-container .layout-header-container .header-left .sidebar-trigger i {
  font-size: 32px;
  cursor: pointer;
  color: #aeb9c2;
  transition: all 0.3s;
}

.layout-main .layout-container .layout-header-container .header-left .sidebar-trigger .t1 {
  transform: rotateZ(-180deg);
}

.layout-main .layout-container .layout-header-container .header-left .custom-bread-crumb {
  margin-left: 10px;
}

.layout-main .layout-container .layout-header-container .header-right {
  flex: 1;
  display: flex;
  justify-content: flex-end;
  align-items: center;
}

.layout-main .layout-container .layout-content-container .content-wrapper {
  position: relative;
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  overflow: auto;
  background: #f5f7f9;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container {
  padding: 15px;
  height: "calc(100% - 80px)";
  overflow: auto;
  /** 搜索工具栏 */
  /** 列表分页工具栏 */
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .el-card__body {
  padding: 10px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar {
  display: flex;
  justify-content: space-between;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .left {
  display: flex;
  justify-content: flex-start;
  align-items: center;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .left .searchForm .el-form-item {
  margin-bottom: 10px;
  margin-right: 0px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .left .searchForm .el-form-item .el-form-item__content {
  line-height: 0;
  margin-right: 10px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .left .searchForm button {
  margin-left: 0;
  margin-right: 10px;
  margin-bottom: 10px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .left .searchForm .el-button-group button {
  margin-right: -1px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .left button {
  margin-left: 0;
  margin-right: 10px;
  margin-bottom: 10px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .left .el-button-group button {
  margin-right: -1px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .right {
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .search-bar .right button {
  margin-bottom: 10px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .btn-action {
  margin-bottom: 10px;
}

.layout-main .layout-container .layout-content-container .content-wrapper .container .pagination {
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 10px;
  overflow: hidden;
}

.simple_search_bar_wrapper .el-input-group__prepend .el-select .el-input {
  width: 120px;
}

.input-with-select .el-input-group__prepend {
  background-color: #fff;
}

.table_action {
  display: flex;
  justify-content: center;
}

.table_action_dropdown .el-dropdown .el-dropdown__caret-button {
  height: 32px;
}

.table_action_dropdown .el-dropdown .el-dropdown-link {
  display: flex;
  align-items: center;
  cursor: pointer;
  color: #409EFF;
  font-size: 12px;
  height: 32px;
}

.table_action_dropdown .el-icon-arrow-down {
  font-size: 12px;
}

.table_popover {
  margin-left: 10px;
  font-size: 12px;
  color: #409EFF;
}

.table_popover_tools {
  display: flex;
  justify-content: center;
  align-items: center;
}

.simple_search_bar_wrapper {
  margin-bottom: 10px;
}

.simple_search_bar_wrapper .searchForm .el-form-item {
  margin-bottom: 0px;
  margin-right: 0px;
}

.simple_search_bar_wrapper .searchForm .el-form-item .el-form-item__content {
  line-height: 0;
}

.simple_search_bar_wrapper .btn_wrapper {
  display: flex;
  justify-content: flex-end;
}

.condition_search_wrapper .conditionSearchForm .el-form-item {
  margin-bottom: 0px;
  margin-right: 0px;
}

.condition_search_wrapper .conditionSearchForm .el-form-item .el-form-item__content {
  line-height: 0;
}

.condition_search_wrapper .search_btn {
  display: flex;
  justify-content: flex-end;
}

.upload .el-upload-list--picture-card .el-upload-list__item-thumbnail {
  height: auto;
}

.amap-sug-result {
  z-index: 9999999;
}

.amap-sug-result .auto-item {
  padding: 8px 5px;
}

.headerBack {
  padding: 20px 20px 20px 20px;
  background: #fff;
}

.operate-container {
  margin-top: 20px;
}

.operate-container .btn-add {
  float: right;
}

.filter_container {
  margin-bottom: 10px;
}

.operate_container {
  margin-bottom: 10px;
}

.operate_container .wrapper {
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.operate_container .wrapper .searchKeyword .el-form-item {
  margin: 0;
}
