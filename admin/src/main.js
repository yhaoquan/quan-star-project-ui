import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import router from '@/router'
import store from '@/store'
// import BinTreeOrg from 'bin-tree-org'
// import Avue from '@smallwei/avue';

import http from '@/libs/http'
import { isAuth } from '@/libs/auth'
import { ResultMessage } from "@/libs/response"
import PubSub from 'pubsub-js'

import * as filters from '@/libs/filters'

// import '@smallwei/avue/lib/index.css';
import 'element-ui/lib/theme-chalk/index.css'
import '@/assets/iconfont/iconfont.css'

import '@/assets/styles/fonts.scss'
import '@/assets/styles/element-ui.scss'
import 'normalize.css'


Vue.use(ElementUI)
// Vue.use(Avue)
// Vue.use(BinTreeOrg)

// 注册全局过滤器
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})


Vue.prototype.$http = http    // 将http注册成全局
Vue.prototype.isAuth = isAuth // 按钮权限认证
Vue.prototype.ResultMessage = ResultMessage // 请求返回提示消息
Vue.prototype.PubSub = PubSub

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
