import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/sysuser/save',
        method: 'post',
        data
    })
}

/**
 * 删除
 * @param {*} idList idList: [1,2,3]
 */
export const deleted = (idList) => {
    return http({
        url: '/sysuser/delete',
        method: 'post',
        data: idList
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/sysuser/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/sysuser/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/sysuser/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/sysuser/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/sysuser/selectMyPage',
        method: 'get',
        params: query
    })
}

/**
 * 获取用户信息
 * @param {*} query 
 */
export const getUserInfo = (query) => {
    return http({
        url: '/sysuser/getUserInfo',
        method: 'get',
        params: query
    })
}


/**
 * 登录验证
 * @param {*} data 
 */
export const login = (data) => {
    return http({
        url: '/sysuser/login',
        method: 'post',
        data
    })
}

/**
 * 用户登出
 */
export const logout = () => {
    return http({
        url: '/sysuser/logout',
        method: 'get'
    })
}

/**
 * 根据用户ID加载用户对应的角色
 * @param {*} userId 
 */
export const getRolesByUserId = (userId) => {
    return http({
        url: '/sysuser/getRolesByUserId/'+userId,
        method: 'get'
    })
}

/**
 * 给用户分配角色
 * @param {*} data 
 */
export const setUserRole = (data) => {
    return http({
        url: '/sysuser/setUserRole',
        method: 'post',
        data
    })
}

/**
 * 重置用户登陆密码为[123456]
 * @param {idList: [1,2,3,4]} data 
 */
export const batchResetPassword = (idList) => {
    return http({
        url: '/sysuser/batchResetPassword',
        method: 'post',
        data: idList
    })
}


/**
 * 修改个人密码，不需要权限验证
 * @param {*} data 
 */
export const updateMyPassword = (data) => {
    return http({
        url: '/sysuser/updateMyPassword',
        method: 'post',
        data
    })
}

/**
 * 修改个人基本信息
 * @param {*} data 
 */
export const updatedMyInfo = (data) => {
    return http({
        url: '/sysuser/updateMyInfo',
        method: 'post',
        data
    })
}

/**
 * 修改用户头像
 * @param {*} data 
 */
export const updateMyAvatar = (data) => {
    return http({
        url: '/sysuser/updateMyAvatar',
        method: 'post',
        data
    })
}


/**
 * 忘记密码，进行邮件通知
 * @param {*} data 
 */
export const forgetPasswordSendMail = (data) => {
    return http({
        url: '/sysuser/forgetPasswordSendMail',
        method: 'post',
        data
    })
}

/**
 * 用户找回密码，重新修改密码
 * @param {*} data 
 */
export const forgetPasswordByUpdate = (data) => {
    return http({
        url: '/sysuser/forgetPasswordByUpdate',
        method: 'post',
        data
    })
}

/**
 * 据用户角色ID获取用户对应的权限关联关系数据
 * @param {*} data roleIds
 */
export const getRolesPermitByUserRoleIds = (data) => {
    return http({
        url: '/sysuser/getRolesPermitByUserRoleIds',
        method: 'post',
        data
    })
}