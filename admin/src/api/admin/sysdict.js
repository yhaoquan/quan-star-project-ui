import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/sysdict/save',
        method: 'post',
        data
    })
}

/**
 * 删除
 * @param {*} idList idList: [1,2,3]
 */
export const deleted = (idList) => {
    return http({
         url: '/sysdict/delete',
        method: 'post',
        data: idList
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/sysdict/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/sysdict/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/sysdict/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/sysdict/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/sysdict/selectMyPage',
        method: 'get',
        params: query
    })
}

/**
 * 根据编码获取字典
 * @param data {codes: ['XL', 'ZC']}
 */
export function getDictByCode(data) {
    return http({
        url: '/sysdict/getDictByCode',
        method: 'post',
        data
    })
}
