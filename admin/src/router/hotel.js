import Main from '@/views/main/main.vue';
import ParentView from '@/components/parent-view/parent-view.vue';
import IFrame from '@/components/iframe/iframe.vue';

/**
 * meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 *  href: 'https://xxx' (default: null) 用于跳转到外部连接
 *  iframe: false  用于跳转到外部连接，打开新的页面
 *  iframe: true  用于跳转到外部连接，在tab中打开外部连接，需设置component: IFrame
 * }
 */

export default [
    {
        path: '/hotel',
        name: 'hotel',
        meta: {
            icon: 'logo-buffer',
            title: '酒店管理',
            access: ['admin:page']
        },
        component: Main,
        children: [
            {
                path: 'category',
                name: 'category',
                meta: {
                    icon: 'logo-buffer',
                    title: '分类维护',
                    access: ['product:systemuser:page']
                },
                component: () => import('@/views/product/pmscategory/index.vue')
            },
            {
                path: 'brand',
                name: 'brand',
                meta: {
                    icon: 'logo-buffer',
                    title: '品牌管理',
                    access: ['admin:systemrole:page']
                },
                component: () => import('@/views/product/pmsbrand/index.vue')
            },
            {
                path: 'attr',
                name: 'attr',
                meta: {
                    icon: 'md-funnel',
                    title: '平台属性',
                    access: ['admin:systemmonitor:page']
                },
                component: ParentView,
                children: [
                    {
                        name: 'attr_group',
                        path: 'attr_group',
                        meta: {
                            icon: 'md-funnel',
                            title: '属性分组',
                            access: ['admin:systemmonitor:page']
                        },
                        component: () => import('@/views/product/pmsattrgroup/index.vue')
                    },
                    {
                        name: 'base_attr',
                        path: 'base_attr',
                        meta: {
                            icon: 'md-funnel',
                            title: '规格参数',
                            access: ['admin:systemmonitor:page']
                        },
                        component: () => import('@/views/product/pmsattr/base_attr.vue')
                    },
                    {
                        name: 'sale_attr',
                        path: 'sale_attr',
                        meta: {
                            icon: 'md-funnel',
                            title: '销售属性',
                            access: ['admin:systemmonitor:page']
                        },
                        component: () => import('@/views/product/pmsattr/sale_attr.vue')
                    }
                ]
            },
            {
                path: 'product_manager',
                name: 'product_manager',
                meta: {
                    icon: 'md-funnel',
                    title: '商品维护',
                    access: ['admin:systemmonitor:page']
                },
                component: ParentView,
                children: [
                    {
                        name: 'spu',
                        path: 'spu',
                        meta: {
                            icon: 'md-funnel',
                            title: 'SPU管理',
                            access: ['admin:systemmonitor:page']
                        },
                        component: () => import('@/views/product/product/spu_info.vue')
                    },
                    {
                        name: 'publish',
                        path: 'publish',
                        meta: {
                            icon: 'md-funnel',
                            title: '发布商品',
                            access: ['admin:systemmonitor:page']
                        },
                        component: () => import('@/views/product/product/spu_add.vue')
                    },
                    {
                        name: 'sku',
                        path: 'sku',
                        meta: {
                            icon: 'md-funnel',
                            title: '商品管理',
                            access: ['admin:systemmonitor:page']
                        },
                        component: () => import('@/views/product/product/sku_info.vue')
                    },

                    {
                        //子页面，不在菜单和面包屑中显示
                        path: 'comment',
                        name: 'comment',
                        meta: {
                            icon: 'logo-buffer',
                            title: '商品评论',
                            hideInBread: true,
                            hideInMenu: true
                        },
                        component: () => import('@/views/product/product/sku_comment.vue')
                    },
                ]
            },
        ]
    }
];
