import Main from '@/views/main/main.vue'
import ParentView from '@/components/parent-view/parent-view.vue'
import IFrame from '@/components/iframe/iframe.vue'

/**
 * meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 *  href: 'https://xxx' (default: null) 用于跳转到外部连接
 *  iframe: false  用于跳转到外部连接，打开新的页面
 *  iframe: true  用于跳转到外部连接，在tab中打开外部连接，需设置component: IFrame
 * }
 */

export default [
    {
        path: '/demo',
        name: '_demo',
        component: Main,
        meta: {
            title: 'DEMO 实例',
            icon: 'el-icon-location',
            access: ['demo:page']
        },
        children: [
            {
                path: 'oss',
                name: 'oss',
                meta: {
                    title: 'oss',
                    icon: 'el-icon-location',
                },
                component: () => import('@/views/demo/oss_upload.vue')
            },
            {
                path: 'gps',
                name: 'gps',
                meta: {
                    title: 'gps',
                    icon: 'el-icon-location',
                },
                component: () => import('@/views/demo/gps.vue')
            },
            {
                path: 'tinymce',
                name: 'tinymce',
                meta: {
                    title: 'Tinymce',
                    icon: 'el-icon-location',
                },
                component: () => import('@/views/demo/tinymce.vue')
            },
            {
                path: 'table',
                name: 'table',
                meta: {
                    title: 'Table',
                    icon: 'el-icon-location',
                },
                component: () => import('@/views/demo/table.vue')
            },
            {
                path: 'demo',
                name: 'demo',
                meta: {
                    title: 'Demo',
                    icon: 'el-icon-location',
                },
                component: () => import('@/views/demo/demo.vue')
            },
            {
                path: 'test',
                name: 'test',
                meta: {
                    title: '测试页面',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/test.vue')
            },
            {
                path: 'upload',
                name: 'upload',
                meta: {
                    title: '文件上传',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/upload.vue')
            },
            {
                path: '高德坐标拾取',
                name: '高德坐标拾取',
                meta: {
                    title: '高德坐标拾取',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/高德坐标拾取.vue')
            },
            {
                path: 'Draggable',
                name: 'Draggable',
                meta: {
                    title: '拖拽',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/Draggable.vue')
            },
            {
                path: 'print',
                name: 'print',
                meta: {
                    title: '打印',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/print.vue')
            },
            {
                path: 'sign',
                name: 'sign',
                meta: {
                    title: 'Sign电子签名',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/Sign电子签名.vue')
            },
            {
                path: 'redisson',
                name: 'redisson',
                meta: {
                    title: 'Redisson分布式锁测试',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/分布式锁测试.vue')
            },
            {
                path: 'sku',
                name: 'sku',
                meta: {
                    title: '生成SKU',
                    icon: 'el-icon-location'
                },
                component: () => import('@/views/demo/sku.vue')
            }
        ]
    }
]