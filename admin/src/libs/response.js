import { Notification, MessageBox, Message, Loading } from 'element-ui'

export function ResultMessage(response, showMsg = true) {
    const result = response
    if (result.code === 200) {
        if (showMsg) {
            Notification({
                type: 'success',
                duration: 2000,
                title: '提示',
                message: result.msg
            })
        }
        return true
    } else if (result.code === 500) {
        Notification({
            type: 'error',
            duration: 2000,
            title: '提示',
            message: result.msg
        })
        return false
    } else if (result.code === 510000) {
        // 参数格式校验失败
        let msg = ''
        Object.values(result.data).forEach(v => {
            msg += '<div style="color:red"><i class="el-icon-warning"></i> ' + v + '</div>'
        })
        MessageBox({
            dangerouslyUseHTMLString: true,
            title: result.msg,
            message: msg
        })
        return false
    } else if (result.code === 600000) {
        MessageBox({
            type: 'warning',
            title: '操作失败',
            message: result.msg
        })
        return false
    } else {
        MessageBox({
            type: 'error',
            title: '程序异常',
            message: result.msg
        })
        return false
    }
}