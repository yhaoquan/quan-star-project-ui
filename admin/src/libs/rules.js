
function required(message = '此项必填') { //必填
    return {required: true, trigger: 'blur', message: message }
}

function money(message = '请输入合法金额，小数只能2位') {//金额
    return {
        validator: (rule, value, callback) => {
            if(value===''){
                callback();
            }
            let reg = /^[0-9]+(.[0-9]{2})?$/;
            if (!reg.test(value)) {
                callback(new Error(message));
            } else {
                callback();
            }
        }, trigger: 'blur' }
}

function email(message = '请输入正确的邮箱地址') {//邮件
    return {
        validator: (rule, value, callback) => {
            if(value===''){
                callback();
            }
            let reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
            if (!reg.test(value)) {
                callback(new Error(message));
            } else {
                callback();
            }
        }, trigger: 'blur' }
}

function number(message = '只能输入数字') {//数字
    return {
        validator: (rule, value, callback) => {
            if(value===''){
                callback();
            }
            let reg = /^[0-9]+$/;
            if (!reg.test(value)) {
                callback(new Error(message));
            } else {
                callback();
            }
        }, trigger: 'blur'
    }
}

function double(message = "只能是数字") {//浮点型数字
    return {
        validator: (rule, value, callback) => {
            let reg = /[0-9]+$/;
            if (!reg.test(value)) {
                callback(new Error(message));
            } else {
                callback();
            }
        }, trigger: 'blur'
    }
}

function numberLim(message = '只能输入正整数,并且长度不能超过5') {//数字限制最大长度
    return {
        validator: (rule, value, callback) => {
            let reg = /^[0-9]{0,5}$/;
            if (!reg.test(value)) {
                callback(new Error(message));
            } else {
                callback();
            }
        }, trigger: 'blur'
    }
}

function english(message = '只能输入英文或数字') {//只能输入英文
    return {
        validator: (rule, value, callback) => {
            let reg = /^[a-zA-Z0-9]+$/;
            if (!reg.test(value)) {
                callback(new Error(message));
            } else {
                callback();
            }
        }, trigger: 'blur'
    }
}

function NumAndStr(message = '只能是小写字母和数字'){//只能小写字母或者数字
    return {
        validator: (rule, value, callback) => {
            let reg = /^[a-z0-9]+$/;
            if (!reg.test(value)) {
                callback(new Error(message));
            } else {
                callback();
            }
        }, trigger: 'blur'
    }

}

function mobile(message = '请输入正确的手机号') {//移动手机号
    return {
        validator: (rule, value, callback) => {
            if(value===''){
                callback();
            }
            else{
                let reg = /^1\d{10}$/;
                if (!reg.test(value)) {
                    callback(new Error(message));
                } else {
                    callback();
                }
            }}, trigger: 'blur'
    }
}

function tel(message = '请输入正确的固定电话') {//固定电话
    return {
        validator: (rule, value, callback) => {
            if (value === '') {
                callback();
            }
            else {
                let reg = /^0\d{2,3}-?\d{7,8}$/;
                if (!reg.test(value)) {
                    callback(new Error(message));
                } else {
                    callback();
                }
            }}, trigger: 'blur'
    }
}

export default {
    required,
    email,
    number,
    double,
    numberLim,
    english,
    NumAndStr,
    mobile,
    tel,
    money
}
