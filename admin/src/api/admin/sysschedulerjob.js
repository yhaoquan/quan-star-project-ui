import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/sysschedulerjob/save',
        method: 'post',
        data
    })
}

/**
 * 删除
 * @param {*} idList idList: [1,2,3]
 */
export const deleted = (idList) => {
    return http({
         url: '/sysschedulerjob/delete',
        method: 'post',
        data: idList
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/sysschedulerjob/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/sysschedulerjob/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/sysschedulerjob/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/sysschedulerjob/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/sysschedulerjob/selectMyPage',
        method: 'get',
        params: query
    })
}

/**
 * 暂停任务
 * @param {*} query jobName, jobGroup
 */
export const pauseJob = (query) => {
    return http({
        url: '/sysschedulerjob/pauseJob',
        method: 'get',
        params: query
    })
}

/**
 * 恢复任务
 * @param {*} query jobName, jobGroup
 */
export const resumeJob = (query) => {
    return http({
        url: '/sysschedulerjob/resumeJob',
        method: 'get',
        params: query
    })
}

/**
 * 触发任务
 * @param {*} query jobName, jobGroup
 */
export const triggerJob = (query) => {
    return http({
        url: '/sysschedulerjob/triggerJob',
        method: 'get',
        params: query
    })
}
