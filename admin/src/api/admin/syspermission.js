import http from '@/libs/http'

/**
 * 添加
 * @param {*} data 
 */
export const save = (data) => {
    return http({
        url: '/syspermission/save',
        method: 'post',
        data
    })
}

/**
 * 删除
 * @param {*} idList idList: [1,2,3]
 */
export const deleted = (idList) => {
    return http({
         url: '/syspermission/delete',
        method: 'post',
        data: idList
    })
}

/**
 * 修改
 * @param {*} data 
 */
export const updated = (data) => {
    return http({
        url: '/syspermission/update',
        method: 'post',
        data
    })
}

/**
 * 信息
 * @param {*} id 
 */
export const info = (id) => {
    return http({
        url: '/syspermission/info/' + id,
        method: 'get'
    })
}

/**
 * 列表
 * @param {查询条件} query 
 */
export const list = (query) => {
    return http({
        url: '/syspermission/list',
        method: 'get',
        params: query
    })
}

/**
 * 分页列表
 * @param {查询条件} query 
 */
export const page = (query) => {
    return http({
        url: '/syspermission/page',
        method: 'get',
        params: query
    })
}

/**
 * 自定义分页列表
 * @param {查询条件} query
 */
export const selectMyPage = (query) => {
    return http({
        url: '/syspermission/selectMyPage',
        method: 'get',
        params: query
    })
}


/**
 * 根据菜单ID加载对应权限
 * @param {查询条件} query 
 */
export const findPermissionByMenuId = (menuId) => {
    return http({
        url: '/syspermission/findPermissionByMenuId/'+menuId,
        method: 'get'
    })
}

/**
 * 初始化菜单权限（页面，添加，修改，删除，详情，查询）
 * @param {*} data 
 */
export const initPermission = (data) => {
    return http({
        url: '/syspermission/initPermission',
        method: 'post',
        data
    })
}